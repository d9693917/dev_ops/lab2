#ifndef PR2_IS_PRIME_H
#define PR2_IS_PRIME_H

bool isPrime(int n);

#endif //PR2_IS_PRIME_H
