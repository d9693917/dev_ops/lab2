#include <gtest/gtest.h>
#include "is_prime.h"

TEST(PrimeTest, SmallPrimes) {
    EXPECT_TRUE(isPrime(2));
    EXPECT_TRUE(isPrime(3));
    EXPECT_TRUE(isPrime(5));
    EXPECT_TRUE(isPrime(7));
}

TEST(PrimeTest, NonPrimes) {
    EXPECT_FALSE(isPrime(1));
    EXPECT_FALSE(isPrime(4));
    EXPECT_FALSE(isPrime(6));
    EXPECT_FALSE(isPrime(8));
}

TEST(PrimeTest, LargePrimes) {
    EXPECT_TRUE(isPrime(997));
    EXPECT_TRUE(isPrime(7919));
}
