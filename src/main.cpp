#include <iostream>
#include "is_prime.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Использование: " << argv[0] << " число\n";
        return EXIT_FAILURE;
    }

    int n = std::atoi(argv[1]);

    if (isPrime(n)) {
        std::cout << n << " - простое число\n";
    } else {
        std::cout << n << " - не является простым числом\n";
    }

    return EXIT_SUCCESS;
}